package com.taraso.groupchatserver.controller;

import com.taraso.groupchatserver.entity.Message;
import com.taraso.groupchatserver.service.MessageService;
import org.bson.types.ObjectId;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/v1/messages")
public record MessageController(MessageService messageService) {

    @PostMapping
    public ResponseEntity<Void> createMessage(@RequestBody Message message) {
        ObjectId id = messageService.save(message);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/group/{id}")
    public ResponseEntity<List<Message>> getMessagesByGroupId(@PathVariable("id") ObjectId id) {
        return ResponseEntity.ok(messageService.findByGroupId(id));
    }
}
