package com.taraso.groupchatserver.controller;

import com.taraso.groupchatserver.entity.FriendRequest;
import com.taraso.groupchatserver.service.FriendService;
import org.bson.types.ObjectId;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/api/v1/friends")
public record FriendController(FriendService friendService) {

    @PostMapping
    public ResponseEntity<Void> createFriendRequest(@RequestBody FriendRequest friendRequest) {
        ObjectId id = friendService.save(friendRequest);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/{id}/accept")
    public ResponseEntity<Void> acceptFriendRequest(@PathVariable("id") ObjectId id) {
        friendService.accept(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/deny")
    public ResponseEntity<Void> denyFriendRequest(@PathVariable("id") ObjectId id) {
        friendService.deny(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/cancel")
    public ResponseEntity<Void> cancelFriendRequest(@PathVariable("id") ObjectId id) {
        friendService.cancel(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{userId}/{friendId}/delete")
    public ResponseEntity<Void> deleteFriend(@PathVariable("userId") ObjectId userId,
                                             @PathVariable("friendId") ObjectId friendId) {
        friendService.deleteFriend(userId, friendId);
        return ResponseEntity.ok().build();
    }
}
