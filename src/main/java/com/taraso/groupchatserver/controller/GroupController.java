package com.taraso.groupchatserver.controller;

import com.taraso.groupchatserver.entity.Group;
import com.taraso.groupchatserver.service.GroupService;
import org.bson.types.ObjectId;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/api/v1/groups")
public record GroupController(GroupService groupService) {

    @PostMapping
    public ResponseEntity<Void> createGroup(@RequestBody Group group) {
        ObjectId id = groupService.save(group);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Group> getGroupById(@PathVariable("id") ObjectId id) {
        return ResponseEntity.ok(groupService.findById(id));
    }
}
