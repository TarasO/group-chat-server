package com.taraso.groupchatserver.controller;

import com.taraso.groupchatserver.entity.JoinRequest;
import com.taraso.groupchatserver.service.JoinService;
import org.bson.types.ObjectId;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/api/v1/join")
public record JoinController(JoinService joinService) {

    @PostMapping
    public ResponseEntity<Void> createJoinRequest(@RequestBody JoinRequest joinRequest) {
        ObjectId id = joinService.save(joinRequest);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/{id}/accept")
    public ResponseEntity<Void> acceptJoinRequest(@PathVariable("id") ObjectId id) {
        joinService.accept(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/deny")
    public ResponseEntity<Void> denyJoinRequest(@PathVariable("id") ObjectId id) {
        joinService.deny(id);
        return ResponseEntity.ok().build();
    }
}
