package com.taraso.groupchatserver.repository;

import com.taraso.groupchatserver.entity.JoinRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JoinRepository extends MongoRepository<JoinRequest, Object> {
}
