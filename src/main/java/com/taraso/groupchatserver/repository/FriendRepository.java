package com.taraso.groupchatserver.repository;

import com.taraso.groupchatserver.entity.FriendRequest;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendRepository extends MongoRepository<FriendRequest, ObjectId> {


}
