package com.taraso.groupchatserver.repository;

import com.taraso.groupchatserver.entity.Message;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends MongoRepository<Message, ObjectId> {
    @Query("{ 'group': { $eq: ?0 } }")
    List<Message> findByGroupId(ObjectId id);
}
