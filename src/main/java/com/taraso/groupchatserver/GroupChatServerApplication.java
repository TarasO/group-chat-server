package com.taraso.groupchatserver;

import com.taraso.groupchatserver.entity.User;
import com.taraso.groupchatserver.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class GroupChatServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GroupChatServerApplication.class, args);
    }

//    @Bean
//    public CommandLineRunner commandLineRunnerBean(UserRepository userRepository, PasswordEncoder passwordEncoder) {
//        return (args) -> {
//            User user1 = userRepository.save(new User("friend1", passwordEncoder.encode("lolkek")));
//            User user2 = userRepository.save(new User("friend2", passwordEncoder.encode("lolkek")));
//
//            User user3 = new User("user", passwordEncoder.encode("lolkek"));
//            user3.getFriends().add(user1);
//            user3.getFriends().add(user2);
//            userRepository.save(user3);
//        };
//    }

}
