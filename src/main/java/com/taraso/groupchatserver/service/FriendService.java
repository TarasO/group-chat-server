package com.taraso.groupchatserver.service;

import com.taraso.groupchatserver.entity.FriendRequest;
import com.taraso.groupchatserver.entity.RequestStatus;
import com.taraso.groupchatserver.entity.User;
import com.taraso.groupchatserver.repository.FriendRepository;
import com.taraso.groupchatserver.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class FriendService {

    private FriendRepository friendRepository;
    private UserRepository userRepository;

    public ObjectId save(FriendRequest friendRequest) {
        friendRequest.setStatus(RequestStatus.PENDING);
        return friendRepository.save(friendRequest).getId();
    }

    @Transactional
    public void accept(ObjectId id) {
        FriendRequest friendRequest = friendRepository.findById(id).orElseThrow(IllegalAccessError::new);
        friendRequest.setStatus(RequestStatus.ACCEPTED);
        friendRepository.save(friendRequest);

        User sender = friendRequest.getSender();
        User receiver = friendRequest.getReceiver();

        sender.getFriends().add(receiver);
        receiver.getFriends().add(sender);

        userRepository.save(sender);
        userRepository.save(receiver);
    }

    public void deny(ObjectId id) {
        FriendRequest friendRequest = friendRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        friendRequest.setStatus(RequestStatus.DENIED);
        friendRepository.save(friendRequest);
    }

    public void cancel(ObjectId id) {
        FriendRequest friendRequest = friendRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        friendRequest.setStatus(RequestStatus.CANCELED);
        friendRepository.save(friendRequest);
    }

    @Transactional
    public void deleteFriend(ObjectId userId, ObjectId friendId) {
        User user = userRepository.findById(userId).orElseThrow(IllegalArgumentException::new);
        User friend = userRepository.findById(friendId).orElseThrow(IllegalArgumentException::new);

        user.getFriends().remove(friend);
        friend.getFriends().remove(user);

        userRepository.save(user);
        userRepository.save(friend);
    }
}
