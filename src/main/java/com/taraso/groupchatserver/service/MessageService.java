package com.taraso.groupchatserver.service;


import com.taraso.groupchatserver.entity.Message;
import com.taraso.groupchatserver.repository.MessageRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public record MessageService(MessageRepository messageRepository) {

    public ObjectId save(Message message) {
        return messageRepository.save(message).getId();
    }

    public List<Message> findByGroupId(ObjectId id) {
        return messageRepository.findByGroupId(id);
    }
}
