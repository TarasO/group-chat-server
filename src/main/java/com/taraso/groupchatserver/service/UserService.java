package com.taraso.groupchatserver.service;

import com.taraso.groupchatserver.entity.User;
import com.taraso.groupchatserver.repository.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public record UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {

    public ObjectId save(User user) {
        if(userRepository.existsByUsername(user.getUsername())) {
            throw new IllegalArgumentException();
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user).getId();
    }

    public User findById(ObjectId id) {
        return userRepository.findById(id)
                             .orElseThrow(NoSuchElementException::new);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(NoSuchElementException::new);
    }

    public ObjectId login(User user) {
        User foundUser = userRepository.findByUsername(user.getUsername()).orElseThrow(NoSuchElementException::new);
        if(passwordEncoder.matches(user.getPassword(), foundUser.getPassword())) {
            return foundUser.getId();
        } else {
            throw new NoSuchElementException();
        }
    }
}
