package com.taraso.groupchatserver.service;

import com.taraso.groupchatserver.entity.Group;
import com.taraso.groupchatserver.repository.GroupRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public record GroupService(GroupRepository groupRepository) {

    public ObjectId save(Group group) {
        return groupRepository.save(group).getId();
    }

    public Group findById(ObjectId id) {
        return groupRepository.findById(id).orElseThrow(NoSuchElementException::new);
    }

}
