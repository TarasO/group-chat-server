package com.taraso.groupchatserver.service;

import com.taraso.groupchatserver.entity.Group;
import com.taraso.groupchatserver.entity.JoinRequest;
import com.taraso.groupchatserver.entity.RequestStatus;
import com.taraso.groupchatserver.entity.User;
import com.taraso.groupchatserver.repository.GroupRepository;
import com.taraso.groupchatserver.repository.JoinRepository;
import com.taraso.groupchatserver.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
public class JoinService {

    private JoinRepository joinRepository;
    private UserRepository userRepository;
    private GroupRepository groupRepository;

    public ObjectId save(JoinRequest joinRequest) {
        joinRequest.setStatus(RequestStatus.PENDING);
        joinRepository.save(joinRequest);
        return joinRequest.getId();
    }

    @Transactional
    public void accept(ObjectId id) {
        JoinRequest joinRequest = joinRepository.findById(id).orElseThrow(NoSuchElementException::new);
        joinRequest.setStatus(RequestStatus.ACCEPTED);
        joinRepository.save(joinRequest);

        Group group = joinRequest.getGroup();
        User receiver = joinRequest.getReceiver();

        group.getMembers().add(receiver);
        receiver.getGroups().add(group);

        groupRepository.save(group);
        userRepository.save(receiver);
    }

    public void deny(ObjectId id) {
        JoinRequest joinRequest = joinRepository.findById(id).orElseThrow(NoSuchElementException::new);
        joinRequest.setStatus(RequestStatus.DENIED);
        joinRepository.save(joinRequest);
    }
}
