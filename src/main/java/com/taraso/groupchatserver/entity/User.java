package com.taraso.groupchatserver.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Document(collection="users")
public class User {

    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId id;

    @Indexed(unique = true)
    String username;
    String password;

    String imageUrl;

    @DocumentReference
    List<User> friends = new ArrayList<>();
    @DocumentReference
    List<Group> groups = new ArrayList<>();
}
