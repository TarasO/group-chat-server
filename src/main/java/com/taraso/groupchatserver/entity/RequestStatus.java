package com.taraso.groupchatserver.entity;

public enum RequestStatus {
    PENDING, ACCEPTED, DENIED, CANCELED
}
