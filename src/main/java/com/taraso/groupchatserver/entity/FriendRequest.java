package com.taraso.groupchatserver.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDateTime;

@Getter
@Setter
@Document
public class FriendRequest {

    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId id;

    @DocumentReference
    User sender;
    @DocumentReference
    User receiver;

    RequestStatus status;

    LocalDateTime timestamp = LocalDateTime.now();
}
