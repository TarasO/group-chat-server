package com.taraso.groupchatserver.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Document
public class Group {

    @JsonSerialize(using = ToStringSerializer.class)
    @Id
    ObjectId id;

    String name;

    @DocumentReference
    User owner;

    @DocumentReference
    List<User> members = new ArrayList<>();
}
